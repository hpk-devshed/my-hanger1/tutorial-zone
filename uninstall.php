<?php
/**
 * Fired when the plugin is uninstalled.
 *
 * @link  https://hyperclock.eu
 * @since 1.0.0
 *
 * @package HTZ
 * @subpackage HTZ/Uninstall
 */

// If uninstall not called from ClassicPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

global $htz;

if ( is_multisite() ) {

	// Get all blogs in the network and activate plugin on each one.
	$blogids = $htz->get_col( //phpcs:ignore WordPress.DB.DirectDatabaseQuery.DirectQuery,WordPress.DB.DirectDatabaseQuery.NoCaching
		"
		SELECT blog_id FROM $htz->blogs
		WHERE archived = '0' AND spam = '0' AND deleted = '0'
	"
	);

	foreach ( $blogids as $blogid ) {
		switch_to_blog( $blogid );
		htz_delete_data();
		restore_current_blog();
	}
} else {
	htz_delete_data();
}


/**
 * Delete Data.
 *
 * @since 1.3.0
 */
function htz_delete_data() {

	$settings = get_option( 'htz_settings' );

	if ( ! empty( $settings['uninstall_options'] ) ) {

		delete_option( 'htz_settings' );

	}

	if ( ! empty( $settings['uninstall_data'] ) ) {

		$htzs = get_posts(
			array(
				'post_type' => 'h_tutorialzone',
			)
		);

		foreach ( $htzs as $htz ) {
			wp_delete_post( $htz->ID );
		}

		htz_delete_taxonomy( 'htz_category' );
		htz_delete_taxonomy( 'htz_tag' );
	}

	// Delete the cache.
	htz_delete_cache();

}


/**
 * Delete Custom Taxonomy.
 *
 * @since 1.3.0
 *
 * @param string $taxonomy Custom taxonomy.
 */
function htz_delete_taxonomy( $taxonomy ) {
	global $wpdb;

	$query = 'SELECT t.name, t.term_id
            FROM ' . $wpdb->terms . ' AS t
            INNER JOIN ' . $wpdb->term_taxonomy . ' AS tt
            ON t.term_id = tt.term_id
            WHERE tt.taxonomy = "' . $taxonomy . '"';

	$terms = $wpdb->get_results( $query ); //phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared,WordPress.DB.DirectDatabaseQuery.DirectQuery,WordPress.DB.DirectDatabaseQuery.NoCaching

	foreach ( $terms as $term ) {
		wp_delete_term( $term->term_id, $taxonomy );
	}
}

/**
 * Delete Cache.
 *
 * @since 1.8.0
 */
function htz_delete_cache() {
	global $wpdb;

	$sql = "
		DELETE FROM {$wpdb->termmeta}
		WHERE `meta_key` LIKE '_htz_cache_%'
	";

	$wpdb->query( $sql ); //phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared,WordPress.DB.DirectDatabaseQuery.DirectQuery,WordPress.DB.DirectDatabaseQuery.NoCaching

}
