<?php
/**
 * Tutorial Zone
 *
 * Tutorial Zone let's you create a tuttorial area
 * or section on your ClassicPress website.
 *
 * @package   HTZ
 * @author    hyperclock
 * @license   GPL-2.0+
 * @link      https://hyperclock.eu
 * @copyright 2020 JMColeman (hyperclock)
 *
 * @wordpress-plugin
 * Plugin Name: Totorial Zone
 * Plugin URI: https://gitlab.com/hpk-devshed/my-hanger1/tutorial-zone
 * Description: Fastest way to create a highly-flexible tutorial area.
 * Version: 1.0.0
 * Author: hyperclock
 * Author URI: https://hyperclock.eu
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: htz
 * Domain Path: /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Holds the filesystem directory path (with trailing slash) for HTZ
 *
 * @since 1.2.0
 *
 * @var string Plugin folder path
 */
if ( ! defined( 'HTZ_PLUGIN_DIR' ) ) {
	define( 'HTZ_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}

/**
 * Holds the filesystem directory path (with trailing slash) for HTZ
 *
 * @since 1.2.0
 *
 * @var string Plugin folder URL
 */
if ( ! defined( 'HTZ_PLUGIN_URL' ) ) {
	define( 'HTZ_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}

/**
 * Holds the filesystem directory path (with trailing slash) for HTZ
 *
 * @since 1.2.0
 *
 * @var string Plugin Root File
 */
if ( ! defined( 'HTZ_PLUGIN_FILE' ) ) {
	define( 'HTZ_PLUGIN_FILE', __FILE__ );
}


/**
 * HTZ Settings
 *
 * @since 1.5.0
 *
 * @var array htz Settings
 */
global $htz_settings;
$htz_settings = htz_get_settings();


/**
 * Get Settings.
 *
 * Retrieves all plugin settings
 *
 * @since  1.2.0
 * @return array htz settings
 */
function htz_get_settings() {

	$settings = get_option( 'htz_settings' );

	/**
	 * Settings array
	 *
	 * Retrieves all plugin settings
	 *
	 * @since 1.2.0
	 * @param array $settings Settings array
	 */
	return apply_filters( 'htz_get_settings', $settings );
}


/*
 *----------------------------------------------------------------------------
 * Include files
 *----------------------------------------------------------------------------
 */

	require_once HTZ_PLUGIN_DIR . 'includes/admin/default-settings.php';
	require_once HTZ_PLUGIN_DIR . 'includes/admin/register-settings.php';
	require_once HTZ_PLUGIN_DIR . 'includes/public/public.php';
	require_once HTZ_PLUGIN_DIR . 'includes/activate-deactivate.php';
	require_once HTZ_PLUGIN_DIR . 'includes/custom-post-type.php';
	require_once HTZ_PLUGIN_DIR . 'includes/main.php';
	require_once HTZ_PLUGIN_DIR . 'includes/shortcode.php';
	require_once HTZ_PLUGIN_DIR . 'includes/search.php';
	require_once HTZ_PLUGIN_DIR . 'includes/feed.php';
	require_once HTZ_PLUGIN_DIR . 'includes/breadcrumbs.php';
	require_once HTZ_PLUGIN_DIR . 'includes/widgets/class-htz-breadcrumb-widget.php';
	require_once HTZ_PLUGIN_DIR . 'includes/widgets/class-htz-sections-widget.php';
	require_once HTZ_PLUGIN_DIR . 'includes/widgets/class-htz-articles-widget.php';
	require_once HTZ_PLUGIN_DIR . 'includes/deprecated.php';


/*
 *----------------------------------------------------------------------------
 * Dashboard and Administrative Functionality
 *----------------------------------------------------------------------------
 */

if ( is_admin() || ( defined( 'WP_CLI' ) && WP_CLI ) ) {

	include_once HTZ_PLUGIN_DIR . 'includes/admin/admin.php';
	include_once HTZ_PLUGIN_DIR . 'includes/admin/settings-page.php';
	include_once HTZ_PLUGIN_DIR . 'includes/admin/save-settings.php';
	include_once HTZ_PLUGIN_DIR . 'includes/admin/help-tab.php';
	include_once HTZ_PLUGIN_DIR . 'includes/admin/modules/cache.php';

}

