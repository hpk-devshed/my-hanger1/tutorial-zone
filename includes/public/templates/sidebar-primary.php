<?php
/**
 * The template for displaying the sidebar.
 *
 * @link  https://webberzone.com
 * @since 1.9.0
 *
 * @package HTZ
 */

?>

<div id="htz-sidebar-primary" class="sidebar">
	<?php if ( is_active_sidebar( 'htz-primary' ) ) : ?>
		<?php dynamic_sidebar( 'htz-primary' ); ?>
	<?php else : ?>
		<aside id="meta" class="widget">
			<h3 class="widget-title"><?php esc_html_e( 'Meta', 'tutorialzone' ); ?></h3>
			<ul>
				<?php wp_register(); ?>
				<li><?php wp_loginout(); ?></li>
				<?php wp_meta(); ?>
			</ul>
		</aside>
	<?php endif; ?>
</div>
