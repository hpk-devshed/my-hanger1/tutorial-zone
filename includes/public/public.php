<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link  https://webberzone.com
 * @since 1.0.0
 *
 * @package    HTZ
 * @subpackage HTZ/public
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 * Initialises text domain for l10n.
 *
 * @since 1.0.0
 */
function htz_lang_init() {
	load_plugin_textdomain( 'htz', false, dirname( plugin_basename( HTZ_PLUGIN_FILE ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'htz_lang_init' );


/**
 * Register Styles and scripts.
 *
 * @since 1.0.0
 */
function wpkb_enqueue_styles() {

	if ( htz_get_option( 'include_styles' ) ) {
		wp_register_style( 'htz_styles', HTZ_PLUGIN_URL . 'includes/public/css/htz-styles.min.css', false, '1.0' );
	}

	wp_add_inline_style( 'htz_styles', esc_html( htz_get_option( 'custom_css' ) ) );

	if ( htz_get_option( 'show_sidebar' ) ) {
		$extra_styles = '.sidebar{width:25%;}.content-area{width:75%;float:left;}';
		wp_add_inline_style( 'htz_styles', $extra_styles );
	}

}
add_action( 'wp_enqueue_scripts', 'wpkb_enqueue_styles' );


/**
 * Replace the archive temlate for the knowledge base. Filters template_include.
 *
 * To further customize these archive views, you may create a
 * new template file for each one in your theme's folder:
 * archive-h_tutorialzone.php (Main KB archives), htz-category.php (Category/Section archives),
 * htz-search.php (Search results page) or taxonomy-htz_tag.php (Tag archives)
 *
 * @since 1.0.0
 *
 * @param  string $template Default Archive Template location.
 * @return string Modified Archive Template location
 */
function htz_archive_template( $template ) {

	$template_name = '';

	if ( is_singular( 'h_tutorialzone' ) ) {
		$template_name = 'single-h_tutorialzone.php';
	}

	if ( is_post_type_archive( 'h_tutorialzone' ) ) {

		if ( is_search() ) {
			$template_name = 'htz-search.php';
		} else {
			$template_name = 'archive-h_tutorialzone.php';
		}
	}

	if ( is_tax( 'htz_category' ) && ! is_search() ) {
		$template_name = 'taxonomy-htz_category.php';
	}

	if ( '' !== $template_name && '' === locate_template( array( $template_name ) ) ) {
		$template = HTZ_PLUGIN_DIR . 'includes/public/templates/' . $template_name;
	}

	return $template;
}
add_filter( 'template_include', 'htz_archive_template' );


/**
 * For knowledge base search results, set posts_per_page 10.
 *
 * @since 1.1.0
 *
 * @param  object $query The search query object.
 * @return object $query Updated search query object
 */
function htz_posts_per_search_page( $query ) {

	if ( ! is_admin() && is_search() && isset( $query->query_vars['post_type'] ) && 'h_tutorialzone' === $query->query_vars['post_type'] ) {
		$query->query_vars['posts_per_page'] = 10;
	}

	return $query;
}
add_filter( 'pre_get_posts', 'htz_posts_per_search_page' );

/**
 * Update the title on HTZ archive.
 *
 * @since 1.6.0
 *
 * @param array $title Title of the page.
 * @return array Updated title
 */
function htz_update_title( $title ) {

	if ( is_post_type_archive( 'h_tutorialzone' ) && ! is_search() ) {

		$title['title'] = htz_get_option( 'kb_title' );
	}

	return $title;
}
add_filter( 'document_title_parts', 'htz_update_title', 99999 );


/**
 * Get the HTML for alert messages
 *
 * @since 2.7.0
 *
 * @param  array  $args Arguments array.
 * @param  string $content Content to wrap in the alert divs.
 * @return string HTML output.
 */
function htz_get_alert( $args = array(), $content ) {

	$defaults = array(
		'type'  => 'primary',
		'class' => 'alert',
		'text'  => '',
	);

	// Parse incomming $args into an array and merge it with $defaults.
	$args = wp_parse_args( $args, $defaults );

	$type = 'htz-alert-' . $args['type'];

	$class = implode( ' ', explode( ',', $args['class'] ) );
	$class = $type . ' ' . $class;

	ob_start();
	?>

	<div class="htz-alert <?php echo $class; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>" role="alert">
	<?php
		echo $args['text']; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		echo do_shortcode( $content ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	?>
	</div>

	<?php

	$html = ob_get_clean();

	/**
	 * Filter the HTML for alert messages
	 *
	 * @since 2.7.0
	 *
	 * @param  string $html HTML for alert messages.
	 * @param  array  $args Arguments array.
	 * @param  string $content Content to wrap in the alert divs.
	 */
	return apply_filters( 'htz_get_alert', $html, $args, $content );
}


/**
 * Register the WZ Knowledge Base sidebars.
 *
 * @since 1.9.0
 */
function htz_register_sidebars() {
	/* Register the 'htz-primary' sidebar. */
	register_sidebar(
		array(
			'id'            => 'htz-primary',
			'name'          => __( 'WZ Knowledge Base Sidebar', 'tutorialzone' ),
			'description'   => __( 'Displays on WZ Knowledge Base templates displayed by the plugin', 'tutorialzone' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
	);
	/* Repeat register_sidebar() code for additional sidebars. */
}
add_action( 'widgets_init', 'htz_register_sidebars' );
