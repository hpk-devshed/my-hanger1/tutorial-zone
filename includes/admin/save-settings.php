<?php
/**
 * Save settings.
 *
 * Functions to register, read, write and update settings.
 * Portions of this code have been inspired by Easy Digital Downloads, WordPress Settings Sandbox, etc.
 *
 * @link  https://webberzone.com
 * @since 1.2.0
 *
 * @package    HTZ
 * @subpackage Admin/Save_Settings
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 * Sanitize the form data being submitted.
 *
 * @since  1.2.0
 * @param  array $input Input unclean array.
 * @return array|bool Sanitized array. False if error.
 */
function htz_settings_sanitize( $input = array() ) {

	// First, we read the options collection.
	global $htz_settings;

	// This should be set if a form is submitted, so let's save it in the $referrer variable.
	if ( empty( $_POST['_wp_http_referer'] ) ) { // phpcs:ignore WordPress.Security.NonceVerification.Missing
		return $input;
	}

	parse_str( sanitize_text_field( wp_unslash( $_POST['_wp_http_referer'] ) ), $referrer ); // phpcs:ignore WordPress.Security.NonceVerification.Missing

	// Get the various settings we've registered.
	$settings_types = htz_get_registered_settings_types();

	// Check if we need to set to defaults.
	$reset = isset( $_POST['settings_reset'] ); // phpcs:ignore WordPress.Security.NonceVerification.Missing

	if ( $reset ) {
		htz_settings_reset();
		$htz_settings = get_option( 'htz_settings' );

		add_settings_error( 'htz-notices', '', __( 'Settings have been reset to their default values. Reload this page to view the updated settings', 'tutorialzone' ), 'error' );

		// Re-register post type and flush the rewrite rules.
		htz_register_post_type();
		flush_rewrite_rules();

		return $htz_settings;
	}

	// Get the tab. This is also our settings' section.
	$tab = isset( $referrer['tab'] ) ? $referrer['tab'] : 'general';

	$input = $input ? $input : array();

	/**
	 * Filter the settings for the tab. e.g. htz_settings_general_sanitize.
	 *
	 * @since  1.2.0
	 * @param  array $input Input unclean array
	 */
	$input = apply_filters( 'htz_settings_' . $tab . '_sanitize', $input );

	// Create out output array by merging the existing settings with the ones submitted.
	$output = array_merge( $htz_settings, $input );

	// Loop through each setting being saved and pass it through a sanitization filter.
	foreach ( $settings_types as $key => $type ) {

		/**
		 * Skip settings that are not really settings.
		 *
		 * @since  1.5.0
		 * @param  array $non_setting_types Array of types which are not settings.
		 */
		$non_setting_types = apply_filters( 'htz_non_setting_types', array( 'header', 'descriptive_text' ) );

		if ( in_array( $type, $non_setting_types, true ) ) {
			continue;
		}

		if ( array_key_exists( $key, $output ) ) {

			/**
			 * Field type filter.
			 *
			 * @since 1.2.0
			 * @param array $output[$key] Setting value.
			 * @param array $key Setting key.
			 */
			$output[ $key ] = apply_filters( 'htz_settings_sanitize_' . $type, $output[ $key ], $key );
		}

		/**
		 * Field type filter for a specific key.
		 *
		 * @since 1.2.0
		 * @param array $output[$key] Setting value.
		 * @param array $key Setting key.
		 */
		$output[ $key ] = apply_filters( 'htz_settings_sanitize' . $key, $output[ $key ], $key );

		// Delete any key that is not present when we submit the input array.
		if ( empty( $input[ $key ] ) ) {
			unset( $output[ $key ] );
		}
	}

	// Delete any settings that are no longer part of our registered settings.
	if ( array_key_exists( $key, $output ) && ! array_key_exists( $key, $settings_types ) ) {
		unset( $output[ $key ] );
	}

	add_settings_error( 'htz-notices', '', __( 'Settings updated.', 'tutorialzone' ), 'updated' );

	// Re-register post type and flush the rewrite rules.
	htz_register_post_type();
	flush_rewrite_rules();

	/**
	 * Filter the settings array before it is returned.
	 *
	 * @since 1.5.0
	 * @param array $output Settings array.
	 * @param array $input Input settings array.
	 */
	return apply_filters( 'htz_settings_sanitize', $output, $input );

}


/**
 * Sanitize text fields
 *
 * @since 1.2.0
 *
 * @param  array $value The field value.
 * @return string  $value  Sanitized value
 */
function htz_sanitize_text_field( $value ) {
	return htz_sanitize_textarea_field( $value );
}
add_filter( 'htz_settings_sanitize_text', 'htz_sanitize_text_field' );


/**
 * Sanitize number fields
 *
 * @since 1.6.0
 *
 * @param  array $value The field value.
 * @return string  $value  Sanitized value
 */
function htz_sanitize_number_field( $value ) {
	return filter_var( $value, FILTER_SANITIZE_NUMBER_INT );
}
add_filter( 'htz_settings_sanitize_number', 'htz_sanitize_number_field' );


/**
 * Sanitize CSV fields
 *
 * @since 1.5.0
 *
 * @param  array $value The field value.
 * @return string  $value  Sanitized value
 */
function htz_sanitize_csv_field( $value ) {

	return implode( ',', array_map( 'trim', explode( ',', sanitize_text_field( wp_unslash( $value ) ) ) ) );
}
add_filter( 'htz_settings_sanitize_csv', 'htz_sanitize_csv_field' );


/**
 * Sanitize CSV fields which hold numbers e.g. IDs
 *
 * @since 1.5.0
 *
 * @param  array $value The field value.
 * @return string  $value  Sanitized value
 */
function htz_sanitize_numbercsv_field( $value ) {

	return implode( ',', array_filter( array_map( 'absint', explode( ',', sanitize_text_field( wp_unslash( $value ) ) ) ) ) );
}
add_filter( 'htz_settings_sanitize_numbercsv', 'htz_sanitize_numbercsv_field' );


/**
 * Sanitize textarea fields
 *
 * @since 1.2.0
 *
 * @param  array $value The field value.
 * @return string  $value  Sanitized value
 */
function htz_sanitize_textarea_field( $value ) {

	global $allowedposttags;

	// We need more tags to allow for script and style.
	$moretags = array(
		'script' => array(
			'type'    => true,
			'src'     => true,
			'async'   => true,
			'defer'   => true,
			'charset' => true,
			'lang'    => true,
		),
		'style'  => array(
			'type'   => true,
			'media'  => true,
			'scoped' => true,
			'lang'   => true,
		),
		'link'   => array(
			'rel'      => true,
			'type'     => true,
			'href'     => true,
			'media'    => true,
			'sizes'    => true,
			'hreflang' => true,
		),
	);

	$allowedtags = array_merge( $allowedposttags, $moretags );

	/**
	 * Filter allowed tags allowed when sanitizing text and textarea fields.
	 *
	 * @since 1.5.0
	 *
	 * @param array $allowedtags Allowed tags array.
	 * @param array $value The field value.
	 */
	$allowedtags = apply_filters( 'htz_sanitize_allowed_tags', $allowedtags, $value );

	return wp_kses( wp_unslash( $value ), $allowedtags );

}
add_filter( 'htz_settings_sanitize_textarea', 'htz_sanitize_textarea_field' );


/**
 * Sanitize checkbox fields
 *
 * @since 1.5.0
 *
 * @param  array $value The field value.
 * @return string|int  $value  Sanitized value
 */
function htz_sanitize_checkbox_field( $value ) {

	$value = ( -1 === (int) $value ) ? 0 : 1;

	return $value;
}
add_filter( 'htz_settings_sanitize_checkbox', 'htz_sanitize_checkbox_field' );


/**
 * Sanitize posttypes fields
 *
 * @since 1.5.0
 *
 * @param  array $value The field value.
 * @return string  $value  Sanitized value
 */
function htz_sanitize_posttypes_field( $value ) {

	$post_types = is_array( $value ) ? array_map( 'sanitize_text_field', wp_unslash( $value ) ) : array( 'post', 'page' );

	return implode( ',', $post_types );
}
add_filter( 'htz_settings_sanitize_posttypes', 'htz_sanitize_posttypes_field' );


/**
 * Modify settings when they are being saved.
 *
 * @since 1.8.0
 *
 * @param  array $settings Settings array.
 * @return string  $settings  Sanitized settings array.
 */
function htz_change_settings_on_save( $settings ) {

	// Delete the cache.
	htz_cache_delete();

	return $settings;
}
add_filter( 'htz_settings_sanitize', 'htz_change_settings_on_save' );
