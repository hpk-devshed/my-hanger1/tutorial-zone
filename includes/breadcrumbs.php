<?php
/**
 * Tutorial Zone Breadcrumbs
 *
 * @link  https://hyperclock.eu
 * @since 1.6.0
 *
 * @package    HTZ
 * @subpackage HTZ/breadcrumbs
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 * Creates the breadcrumb.
 *
 * @since 1.6.0
 *
 * @param  array $args Parameters array.
 * @return string|bool Formatted shortcode output. False if not a HTZ post type archive or post.
 */
function htz_get_breadcrumb( $args = array() ) {

	$defaults = array(
		'separator' => ' &raquo; ', // Separator.
	);

	// Parse incomming $args into an array and merge it with $defaults.
	$args = wp_parse_args( $args, $defaults );

	// Return if not a HTZ post type archive or single page.
	if ( ! is_post_type_archive( 'h_tutorialzone' ) && ! is_singular( 'h_tutorialzone' ) && ! is_tax( 'htz_category' ) && ! is_tax( 'htz_tag' ) ) {
		return false;
	}

	$output = '<div class="htz_breadcrumb">';

	// First output the link to home page.
	$output .= '<a href="' . get_option( 'home' ) . '">';
	$output .= esc_html__( 'Home', 'tutorialzone' );
	$output .= '</a>';
	$output .= $args['separator'];

	// Link to the knowledge base.
	$output .= '<a href="' . get_post_type_archive_link( 'h_tutorialzone' ) . '" >' . htz_get_option( 'kb_title' ) . '</a>';

	// Output the category or tag.
	if ( is_tax( 'htz_category' ) || is_tax( 'htz_tag' ) ) {
		$tax = get_queried_object();

		$output .= $args['separator'];
		$output .= htz_breadcrumb_tax_loop( $tax, $args );
	}

	// Output link to single post.
	if ( is_singular( 'h_tutorialzone' ) ) {
		$post = get_queried_object();

		$terms = get_the_terms( $post, 'htz_category' );
		if ( $terms && ! is_wp_error( $terms ) ) {
			$tax     = $terms[0];
			$output .= $args['separator'];
			$output .= htz_breadcrumb_tax_loop( $tax, $args );
		}

		$output .= $args['separator'];
		$output .= '<a href="' . get_permalink( $post ) . '" >' . $post->post_title . '</a>';
	}

	$output .= '</div>'; // End htz_breadcrumb.
	$output .= '<div class="htz_clear"></div>';

	/**
	 * Filter the formatted shortcode output.
	 *
	 * @since 1.6.0
	 *
	 * @param string $output Formatted HTML output
	 * @param array $args Parameters array
	 */
	return apply_filters( 'htz_get_breadcrumb', $output, $args );
}

/**
 * Echo the breadcrumb output.
 *
 * @since 1.6.0
 *
 * @param  array $args Parameters array.
 */
function htz_breadcrumb( $args = array() ) {
	echo htz_get_breadcrumb( $args ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
}

/**
 * Generates the HTML for the taxonomy and its children for the breadcrumb.
 *
 * @since 1.6.0
 *
 * @param object $taxonomy Taxonomy object.
 * @param array  $args     Parameters array.
 * @return string HTML output
 */
function htz_breadcrumb_tax_loop( $taxonomy, $args = array() ) {

	$output = '<a href="' . get_term_link( $taxonomy ) . '" title="' . $taxonomy->name . '" >' . $taxonomy->name . '</a>';

	if ( ! empty( $taxonomy->parent ) ) {
		$output = htz_breadcrumb_tax_loop( get_term( $taxonomy->parent, $taxonomy->taxonomy ), $args ) . $args['separator'] . $output;
	}

	/**
	 * Filters the HTML for the taxonomy and its children for the breadcrumb.
	 *
	 * @since 1.6.0
	 *
	 * @param string $output   HTML output.
	 * @param object $taxonomy Taxonomy object.
	 * @param array  $args     Parameters array.
	 */
	return apply_filters( 'htz_breadcrumb_tax_loop', $output, $taxonomy, $args );
}
