<?php
/**
 * Deprecated functions and variables
 *
 * @link  https://webberzone.com
 * @since 1.5.0
 *
 * @package    HTZ
 * @subpackage HTZ/deprecated
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 * HTZ Settings
 *
 * @since 1.2.0
 * @deprecated 1.5.0
 *
 * @var array HTZ Settings
 */
global $htz_options;
$htz_options = $htz_settings;

