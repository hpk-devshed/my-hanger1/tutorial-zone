<?php
/**
 * The file holds the main plugin function.
 *
 * @link  https://webberzone.com
 * @since 1.0.0
 *
 * @package HTZ
 * @subpackage HTZ/main
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 * The main function to generate the output.
 *
 * @since 1.0.0
 *
 * @param  array $args Parameters array.
 * @return string Formatted output
 */
function htz_knowledge( $args = array() ) {

	$defaults = array(
		'category' => false, // Create a knowledge base for subcategories of this parent ID.
	);

	// Parse incomming $args into an array and merge it with $defaults.
	$args = wp_parse_args( $args, $defaults );

	$output = '<div class="htz">';

	// Are we trying to display a category?
	$level          = ( 0 < $args['category'] ) ? 1 : 0;
	$term_id        = ( 0 < $args['category'] ) ? $args['category'] : 0;
	$nested_wrapper = ( isset( $args['nested_wrapper'] ) ) ? $args['nested_wrapper'] : true;

	$output .= htz_looper( $term_id, $level, $nested_wrapper );

	$output .= '</div>'; // End htz_section.
	$output .= '<div class="htz_clear"></div>';

	/**
	 * Filter the formatted shortcode output.
	 *
	 * @since 1.0.0
	 *
	 * @param string $output Formatted HTML output
	 * @param array $args Parameters array
	 */
	return apply_filters( 'htz_knowledge', $output, $args );

}


/**
 * Creates the knowledge base loop.
 *
 * @since 1.0.0
 *
 * @param  int  $term_id Term ID.
 * @param  int  $level  Level of the loop.
 * @param  bool $nested Run recursive loops before closing HTML wrappers.
 * @return string Formatted output
 */
function htz_looper( $term_id, $level, $nested = true ) {

	$divclasses = array( 'htz_section', 'htz-section-level-' . $level );

	if ( (int) htz_get_option( 'category_level' ) - 1 === $level ) {
		$divclasses[] = 'section group';
	} elseif ( (int) htz_get_option( 'category_level' ) === $level ) {
		$divclasses[] = 'col span_1_of_' . htz_get_option( 'columns', 2 );
	}

	/**
	 * Filter to add more classes if needed.
	 *
	 * @since 1.1.0
	 *
	 * @param array $divclasses Current array of classes
	 * @param int  $level  Level of the loop
	 * @param int  $term_id  Term ID
	 */
	$divclasses = apply_filters( 'htz_loop_div_class', $divclasses, $level, $term_id );

	$output = '<div class="' . implode( ' ', $divclasses ) . '">';

	$term = get_term( $term_id, 'htz_category' );

	if ( ! empty( $term ) && ! is_wp_error( $term ) ) {
		$output .= htz_article_header( $term, $level );
		$output .= htz_list_posts_by_term( $term, $level );
	}

	$output .= '<div class="htz_section_wrapper">';

	// Get Knowledge Base Sections.
	$sections = get_terms(
		'htz_category',
		array(
			'orderby'    => 'slug',
			'hide_empty' => htz_get_option( 'show_empty_sections' ) ? 0 : 1,
			'parent'     => $term_id,
		)
	);

	if ( ! $nested ) {
		$output .= '</div>'; // End htz_section_wrapper.
		$output .= '</div>'; // End htz_section.
	}

	if ( ! empty( $sections ) && ! is_wp_error( $sections ) ) {

		$level++;

		foreach ( $sections as $section ) {
			$output .= htz_looper( $section->term_id, $level );
		}
	}

	if ( $nested ) {
		$output .= '</div>'; // End htz_section_wrapper.
		$output .= '</div>'; // End htz_section.
	}

	/**
	 * Filter the formatted shortcode output.
	 *
	 * @since 1.0.0
	 *
	 * @param string $output Formatted HTML output
	 * @param int  $term_id Term ID
	 * @param int  $level  Level of the loop
	 */
	return apply_filters( 'htz_looper', $output, $term, $level );

}


/**
 * Returns query results for a specific term.
 *
 * @since 1.1.0
 *
 * @param  object $term The Term.
 * @return object Query results for the give term
 */
function htz_query_posts( $term ) {

	// Get the term children for the current term.
	$termchildren = get_term_children( $term->term_id, 'htz_category' );

	// Get all the posts for the current term excluding posts located in its child terms.
	$args = array(
		'posts_per_page' => -1,
		'tax_query'      => array( // phpcs:ignore WordPress.DB.SlowDBQuery.slow_db_query_tax_query
			'relation' => 'AND',
			array(
				'taxonomy' => 'htz_category',
				'field'    => 'id',
				'terms'    => $term->term_id,
			),
			array(
				'taxonomy' => 'htz_category',
				'field'    => 'id',
				'terms'    => $termchildren,
				'operator' => 'NOT IN',
			),
		),
	);

	// Support caching to speed up retrieval.
	if ( ! empty( htz_get_option( 'cache' ) ) ) {

		$meta_key = htz_cache_get_key( $args );

		$query = get_term_meta( $term->term_id, $meta_key, true );
	}

	if ( empty( $query ) ) {
		$query = new WP_Query( $args );
	}

	// Support caching to speed up retrieval.
	if ( ! empty( htz_get_option( 'cache' ) ) ) {
		add_term_meta( $term->term_id, $meta_key, $query, true );
	}

	/**
	 * Filters query results of the specific term.
	 *
	 * @since 1.1.0
	 *
	 * @param object $query Query results for the give term
	 * @param array $args Arguments for WP_Query
	 * @param object $term The Term
	 */
	return apply_filters( 'htz_query_posts', $query, $args, $term );

}


/**
 * Formatted output of posts for a given term.
 *
 * @since 1.1.0
 *
 * @param  object $term  Current term.
 * @param  int    $level Current level in the recursive loop.
 * @return string Formatted output of posts for a given term
 */
function htz_list_posts_by_term( $term, $level ) {

	$output = '';

	$query = htz_query_posts( $term );

	if ( $query->have_posts() ) {

		$output .= htz_article_loop( $term, $level, $query );
		$output .= htz_article_footer( $term, $level, $query );

		wp_reset_postdata();

	}

	/**
	 * Filter the header of the article list.
	 *
	 * @since 1.1.0
	 *
	 * @param string $output Formatted footer output
	 * @param object $term Current term
	 * @param int  $level Current level in the recursive loop
	 * @param object $query Query results object
	 */
	return apply_filters( 'htz_list_posts_by_term', $output, $term, $level, $query );

}

/**
 * Header of the articles list.
 *
 * @since 1.1.0
 *
 * @param  object $term  Current term.
 * @param  int    $level Current level in the recursive loop.
 * @return string Formatted footer output
 */
function htz_article_header( $term, $level ) {

	$output = '<h3 class="htz_section_name htz-section-name-level-' . $level . '">';

	if ( htz_get_option( 'clickable_section', true ) ) {
		$output .= '<a href="' . get_term_link( $term ) . '" title="' . $term->name . '" >' . $term->name . '</a>';
	} else {
		$output .= $term->name;
	}

	if ( $level > 1 && htz_get_option( 'show_article_count', false ) ) {
		$output .= '<div class="htz_section_count">' . $term->count . '</div>';
	}

	$output .= '</h3> ';

	/**
	 * Filter the header of the article list.
	 *
	 * @since 1.1.0
	 *
	 * @param string $output Formatted footer output
	 * @param object $term Current term
	 * @param int  $level Current level in the recursive loop
	 * @param object $query Query results object
	 */
	return apply_filters( 'htz_article_header', $output, $term, $level );

}


/**
 * Creates the list of articles for a particular query results object.
 *
 * @since 1.1.0
 *
 * @param  object $term  Current term.
 * @param  int    $level Current level in the recursive loop.
 * @param  object $query Query results object.
 * @return string Formatted ul loop
 */
function htz_article_loop( $term, $level, $query ) {

	$limit = 0;

	$output = '<ul class="htz-articles-list term-' . $term->term_id . '">';

	while ( $query->have_posts() ) :
		$query->the_post();

		$output .= '<li class="htz-article-name post-' . get_the_ID() . '">';
		$output .= '<a href="' . get_permalink( get_the_ID() ) . '" rel="bookmark" title="' . get_the_title( get_the_ID() ) . '">' . get_the_title( get_the_ID() ) . '</a>';
		if ( htz_get_option( 'show_excerpt', false ) ) {
			$output .= '<div class="htz-article-excerpt post-' . get_the_ID() . '" >' . get_the_excerpt( get_the_ID() ) . '</div>';
		}
		$output .= '</li>';

		$limit++;

		if ( $limit >= htz_get_option( 'limit' ) && ! is_tax( 'htz_category', $term->term_id ) ) {
			break;
		}

	endwhile;

	$output .= '</ul>';

	/**
	 * Filters formatted articles list.
	 *
	 * @since 1.1.0
	 *
	 * @param string $output Formatted ul loop
	 * @param object $term Current term
	 * @param int  $level Current level in the recursive loop
	 * @param object $query Query results object
	 */
	return apply_filters( 'htz_article_loop', $output, $term, $level, $query );

}


/**
 * Footer of the articles list.
 *
 * @since 1.1.0
 *
 * @param  object $term  Current term.
 * @param  int    $level Current level in the recursive loop.
 * @param  object $query Query results object.
 * @return string Formatted footer output
 */
function htz_article_footer( $term, $level, $query ) {

	$output = '';

	if ( $query->found_posts > htz_get_option( 'limit' ) && ! is_tax( 'htz_category', $term->term_id ) ) {

		$excerpt_more = __( 'Read more articles in ', 'htz' );

		/**
		 * Filters the string in the "more" link displayed in the trimmed articles list.
		 *
		 * @since 1.9.0
		 *
		 * @param string $excerpt_more The string shown before the more link.
		 */
		$excerpt_more = apply_filters( 'htz_excerpt_more', $excerpt_more );

		$output .= '
		<p class="htz-article-footer">' . __( 'Read more articles in ', 'htz' ) . '
			<a href="' . get_term_link( $term ) . '" title="' . $term->name . '" >' . $term->name . '</a> &raquo;
		</p>
		';
	}

	/**
	 * Filter the footer of the article footer.
	 *
	 * @since 1.1.0
	 *
	 * @param string $output Formatted footer output
	 * @param string $output Formatted ul loop
	 * @param object $term Current term
	 * @param int  $level Current level in the recursive loop
	 */
	return apply_filters( 'htz_article_footer', $output, $term, $level, $query );

}

/**
 * Get the meta key based on a list of parameters.
 *
 * @since 1.8.0
 *
 * @param array $attr   Array of attributes.
 * @return string Cache meta key
 */
function htz_cache_get_key( $attr ) {

	$meta_key = '_htz_cache_' . md5( wp_json_encode( $attr ) );

	return $meta_key;
}
