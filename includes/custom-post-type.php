<?php
/**
 * Tutorial Zone Custom Post Type.
 *
 * @link  https://hyperclock.eu
 * @since 1.0.0
 *
 * @package    HTZ
 * @subpackage HTZ/CPT
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 * Register htz Post Type.
 *
 * @since 1.0.0
 */
function wzkb_register_post_type() {

	$slug     = wzkb_get_option( 'kb_slug', 'tutorialzone' );
	$archives = defined( 'HTZ_DISABLE_ARCHIVE' ) && HTZ_DISABLE_ARCHIVE ? false : $slug;
	$rewrite  = defined( 'HTZ_DISABLE_REWRITE' ) && HTZ_DISABLE_REWRITE ? false : array(
		'slug'       => $slug,
		'with_front' => false,
		'feeds'      => wzkb_get_option( 'disable_kb_feed' ) ? false : true,
	);

	$ptlabels = array(
		'name'               => _x( 'htz', 'Post Type General Name', 'tutorialzone' ),
		'singular_name'      => _x( 'htz', 'Post Type Singular Name', 'tutorialzone' ),
		'menu_name'          => __( 'htz', 'tutorialzone' ),
		'name_admin_bar'     => __( 'htz Article', 'tutorialzone' ),
		'parent_item_colon'  => __( 'Parent Article', 'tutorialzone' ),
		'all_items'          => __( 'All Articles', 'tutorialzone' ),
		'add_new_item'       => __( 'Add New Article', 'tutorialzone' ),
		'add_new'            => __( 'Add New Article', 'tutorialzone' ),
		'new_item'           => __( 'New Article', 'tutorialzone' ),
		'edit_item'          => __( 'Edit Article', 'tutorialzone' ),
		'update_item'        => __( 'Update Article', 'tutorialzone' ),
		'view_item'          => __( 'View Article', 'tutorialzone' ),
		'search_items'       => __( 'Search Article', 'tutorialzone' ),
		'not_found'          => __( 'Not found', 'tutorialzone' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'tutorialzone' ),
	);

	/**
	 * Filter the labels of the post type.
	 *
	 * @since 1.2.0
	 *
	 * @param array $ptlabels Post type lables
	 */
	$ptlabels = apply_filters( 'wzkb_post_type_labels', $ptlabels );

	$ptargs = array(
		'label'         => __( 'h_tutorialzone', 'tutorialzone' ),
		'description'   => __( 'htz', 'tutorialzone' ),
		'labels'        => $ptlabels,
		'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'revisions', 'author', 'custom-fields', 'comments' ),
		'show_in_rest'  => true,
		'taxonomies'    => array( 'wzkb_category', 'wzkb_tag' ),
		'public'        => true,
		'hierarchical'  => false,
		'menu_position' => 5,
		'menu_icon'     => 'dashicons-book-alt',
		'map_meta_cap'  => true,
		'has_archive'   => $archives,
		'rewrite'       => $rewrite,
	);

	/**
	 * Filter the arguments passed to register the post type.
	 *
	 * @since 1.2.0
	 *
	 * @param array $ptargs Post type arguments
	 */
	$ptargs = apply_filters( 'wzkb_post_type_args', $ptargs );

	register_post_type( 'h_tutorialzone', $ptargs );

}
add_action( 'init', 'wzkb_register_post_type' );


/**
 * Register Knowledgebase Custom Taxonomies.
 *
 * @since 1.0.0
 */
function wzkb_register_taxonomies() {

	$catslug = wzkb_get_option( 'category_slug', 'section' );
	$tagslug = wzkb_get_option( 'tag_slug', 'kb-tags' );

	$args = array(
		'hierarchical'      => true,
		'show_admin_column' => true,
		'show_tagcloud'     => false,
		'show_in_rest'      => true,
		'rewrite'           => array(
			'slug'         => $catslug,
			'with_front'   => true,
			'hierarchical' => true,
		),
	);

	// Now register categories for the htz.
	$catlabels = array(
		'name'                       => _x( 'Sections', 'Taxonomy General Name', 'tutorialzone' ),
		'singular_name'              => _x( 'Section', 'Taxonomy Singular Name', 'tutorialzone' ),
		'menu_name'                  => __( 'Sections', 'tutorialzone' ),
		'all_items'                  => __( 'All Sections', 'tutorialzone' ),
		'parent_item'                => __( 'Parent Section', 'tutorialzone' ),
		'parent_item_colon'          => __( 'Parent Section:', 'tutorialzone' ),
		'new_item_name'              => __( 'New Section Name', 'tutorialzone' ),
		'add_new_item'               => __( 'Add New Section', 'tutorialzone' ),
		'edit_item'                  => __( 'Edit Section', 'tutorialzone' ),
		'update_item'                => __( 'Update Section', 'tutorialzone' ),
		'view_item'                  => __( 'View Section', 'tutorialzone' ),
		'separate_items_with_commas' => __( 'Separate sections with commas', 'tutorialzone' ),
		'add_or_remove_items'        => __( 'Add or remove sections', 'tutorialzone' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'tutorialzone' ),
		'popular_items'              => __( 'Popular Sections', 'tutorialzone' ),
		'search_items'               => __( 'Search Sections', 'tutorialzone' ),
		'not_found'                  => __( 'Not Found', 'tutorialzone' ),
		'no_terms'                   => __( 'No sections', 'tutorialzone' ),
		'items_list'                 => __( 'Sections list', 'tutorialzone' ),
		'items_list_navigation'      => __( 'Sections list navigation', 'tutorialzone' ),
	);

	/**
	 * Filter the labels of the custom categories.
	 *
	 * @since 1.2.0
	 *
	 * @param array $catlabels Category labels
	 */
	$args['labels'] = apply_filters( 'wzkb_cat_labels', $catlabels );

	register_taxonomy(
		'wzkb_category',
		array( 'h_tutorialzone' ),
		/**
		 * Filter the arguments of the custom categories.
		 *
		 * @since 1.2.0
		 *
		 * @param array $catlabels Category labels
		 */
		apply_filters( 'wzkb_cat_args', $args )
	);

	// Now register tags for the htz.
	$taglabels = array(
		'name'          => _x( 'Tags', 'Taxonomy General Name', 'tutorialzone' ),
		'singular_name' => _x( 'Tag', 'Taxonomy Singular Name', 'tutorialzone' ),
		'menu_name'     => __( 'Tags', 'tutorialzone' ),
	);

	/**
	 * Filter the labels of the custom tags.
	 *
	 * @since 1.2.0
	 *
	 * @param array $taglabels Tags labels
	 */
	$args['labels'] = apply_filters( 'wzkb_tag_labels', $taglabels );

	$args['hierarchical']    = false;
	$args['show_tagcloud']   = true;
	$args['rewrite']['slug'] = $tagslug;

	register_taxonomy(
		'wzkb_tag',
		array( 'h_tutorialzone' ),
		/**
		 * Filter the arguments of the custom tags.
		 *
		 * @since 1.2.0
		 *
		 * @param array $args Tag arguments
		 */
		apply_filters( 'wzkb_tag_args', $args )
	);

}
add_action( 'init', 'wzkb_register_taxonomies' );

