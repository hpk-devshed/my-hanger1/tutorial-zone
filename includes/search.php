<?php
/**
 * Search for Knowledge Base articles.
 *
 * @link  https://webberzone.com
 * @since 1.0.0
 *
 * @package    HTZ
 * @subpackage HTZ/search
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 * Display custom search form for HTZ.
 *
 * @since 1.1.0
 *
 * @param  boolean $echo Default to echo and not return the form.
 * @return string|null   String when retrieving, null when displaying or if searchform.php exists.
 */
function htz_get_search_form( $echo = true ) {

	$form = '
    <form role="search" method="get" class="htz-search-form" action="' . esc_url( home_url( '/' ) ) . '">
        <label>
            <span class="screen-reader-text">' . _x( 'Search for:', 'label', 'tutorialzone' ) . '</span>
            <input type="search" class="htz-search-field" placeholder="' . esc_attr_x( 'Search the tutorialzone &hellip;', 'placeholder', 'tutorialzone' ) . '" value="' . get_search_query() . '" name="s" title="' . esc_attr_x( 'Search for:', 'label', 'tutorialzone' ) . '" />
        </label>
        <input type="hidden" name="post_type" value="h_tutorialzone">
        <input type="submit" class="htz-search-submit" value="' . esc_attr_x( 'Search', 'submit button', 'tutorialzone' ) . '" />
    </form>
    ';

	/**
	 * Filter the HTML output of the search form.
	 *
	 * @since 1.1.0
	 *
	 * @param string $form The search form HTML output.
	 */
	$result = apply_filters( 'htz_get_search_form', $form );

	if ( null === $result ) {
		$result = $form;
	}

	if ( $echo ) {
		echo $result; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	} else {
		return $result;
	}
}


