<?php
/**
 * Knowledge Base Feed functions
 *
 * @link  https://webberzone.com
 * @since 1.4.0
 *
 * @package    HTZ
 * @subpackage HTZ/shortcode
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Include KB articles in the main feed.
 *
 * @since 1.4.0
 *
 * @param object $query Query object.
 * @return object Filtered Query
 */
function htz_in_feed( $query ) {

	if ( isset( $query['feed'] ) && htz_get_option( 'include_in_feed', false ) ) {
		if ( isset( $query['post_type'] ) ) {

			if ( isset( $_SERVER['REQUEST_URI'] ) && false !== strpos( esc_url( wp_unslash( $_SERVER['REQUEST_URI'] ) ), htz_get_option( 'kb_slug' ) ) ) { // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
				$query['post_type'] = array( 'h_tutorialzone' );
			} else {
				$query['post_type'] = array_merge( (array) $query['post_type'], array( 'h_tutorialzone' ) );
			}
		} else {
			$query['post_type'] = array( 'post', 'h_tutorialzone' );
		}
	}
	return $query;
}
add_filter( 'request', 'htz_in_feed', 11 );

