<?php
/**
 * Knowledge Base Shortcodes
 *
 * @link  https://webberzone.com
 * @since 1.0.0
 *
 * @package    HTZ
 * @subpackage HTZ/shortcode
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 * Create the shortcode to display the KB using [tutorialzone].
 *
 * @since 1.0.0
 *
 * @param  array  $atts    Shortcode attributes array.
 * @param  string $content Content to wrap in the Shortcode.
 * @return string $output Formatted shortcode output
 */
function htz_shortcode( $atts, $content = null ) {

	wp_enqueue_style( 'htz_styles' );
	wp_enqueue_style( 'dashicons' );

	$atts = shortcode_atts(
		array(
			'category' => false,
		),
		$atts,
		'tutorialzone'
	);

	$output = htz_knowledge( $atts );

	/**
	 * Filters tutorialzone shortcode.
	 *
	 * @since 1.0.0
	 *
	 * @return string $output  Formatted shortcode output
	 * @param  array $att  Shortcode attributes array
	 * @param  string $content Content to wrap in the Shortcode
	 */
	return apply_filters( 'htz_shortcode', $output, $atts, $content );
}
add_shortcode( 'tutorialzone', 'htz_shortcode' );


/**
 * Create the shortcode to display the search form using [kbsearch].
 *
 * @since 1.2.0
 *
 * @param  array  $atts    Shortcode attributes array.
 * @param  string $content Content to wrap in the Shortcode.
 * @return string $output Formatted shortcode output
 */
function htz_shortcode_search( $atts, $content = null ) {

	$atts = shortcode_atts(
		array(
			'echo' => false,
		),
		$atts,
		'kbsearch'
	);

	$output = htz_get_search_form( $atts['echo'] );

	/**
	 * Filters knowledge base search form shortcode.
	 *
	 * @since 1.2.0
	 *
	 * @return string $output  Formatted shortcode output
	 * @param  array $att  Shortcode attributes array
	 * @param  string $content Content to wrap in the Shortcode
	 */
	return apply_filters( 'htz_shortcode_search', $output, $atts, $content );
}
add_shortcode( 'kbsearch', 'htz_shortcode_search' );


/**
 * Create the shortcode to display the breadcrumb using [kbbreadcrumb].
 *
 * @since 1.6.0
 *
 * @param  array  $atts    Shortcode attributes array.
 * @param  string $content Content to wrap in the Shortcode.
 * @return string $output Formatted shortcode output
 */
function htz_shortcode_breadcrumb( $atts, $content = null ) {

	$atts = shortcode_atts(
		array(
			'separator' => ' &raquo; ', // Separator.
		),
		$atts,
		'kbbreadcrumb'
	);

	$output = htz_get_breadcrumb( $atts );

	/**
	 * Filters knowledge base breadcrumb shortcode.
	 *
	 * @since 1.6.0
	 *
	 * @return string $output  Formatted shortcode output
	 * @param  array $att  Shortcode attributes array
	 * @param  string $content Content to wrap in the Shortcode
	 */
	return apply_filters( 'htz_shortcode_breadcrumb', $output, $atts, $content );
}
add_shortcode( 'kbbreadcrumb', 'htz_shortcode_breadcrumb' );


/**
 * Create the shortcode to display alerts using [kbalert].
 *
 * @since 1.7.0
 *
 * @param  array  $atts    Shortcode attributes array.
 * @param  string $content Content to wrap in the Shortcode.
 * @return string $output Formatted shortcode output
 */
function htz_shortcode_alert( $atts, $content = null ) {

	wp_enqueue_style( 'htz_styles' );

	$atts = shortcode_atts(
		array(
			'type'  => 'primary',
			'class' => 'alert',
			'text'  => '',
		),
		$atts,
		'kbalert'
	);

	$output = htz_get_alert( $atts, $content );

	/**
	 * Filters knowledge base breadcrumb shortcode.
	 *
	 * @since 1.7.0
	 *
	 * @return string $output  Formatted shortcode output
	 * @param  array $att  Shortcode attributes array
	 * @param  string $content Content to wrap in the Shortcode
	 */
	return apply_filters( 'htz_shortcode_alert', $output, $atts, $content );
}
add_shortcode( 'kbalert', 'htz_shortcode_alert' );


