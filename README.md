# Tutorial Zone

 will allow you to create a tutorial area or section on your ClassicPress project.

The plugin was ok, but din't fit just right, so after I tried several free plugins and themes out there and that couldn't fit my purpose. I decided to fork the **Webber Zone Knowledge Base**, it was the closest to what I wanted. It's designed to be very easy to install and use out of the box and I'll be adding more features into the core and as addons.

The plugin uses a custom post in conjunction with custom taxonomies to create and display your knowledge base.


***Original Base Info:***

>### WebberZone Knowledge Base
>
>__Requires:__ 4.8
>
>__Tested up to:__ 5.3
>
>__License:__ [GPL-2.0+](http://www.gnu.org/licenses/gpl-2.0.html)
>
>__Plugin page:__ [Knowledge Base](https://webberzone.com/plugins/knowledgebase/) | [WordPress.org Plugin page](https://wordpress.org/plugins/knowledgebase/)
>
>Quickly and efficiently create a highly-flexible knowledge base or FAQ on your WordPress blog.
>



